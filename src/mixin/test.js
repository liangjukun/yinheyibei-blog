/*
 * @Descripttion: 
 * @Author: 银河以北
 * @Date: 2024-04-09 18:01:09
 * @LastEditors: 银河以北
 * @LastEditTime: 2024-04-09 18:01:17
 */
export default {
    data() {
        return {
            msg: 'xx'
        }
    },
    created() {
        console.log("hello vue")
    },
    methods: {
        comMethod1: function () {
            console.log('共同方法1');
        },
        comMethod2: function () {
            console.log('共同方法2');
        },
        comMethod3: function () {
            console.log('共同方法3');
        }
    }
}
