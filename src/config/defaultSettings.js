/*
 * @Descripttion: 前台基本默认配置
 * @Author: 银河以北
 * @Date: 2021-06-11 19:53:27
 * @LastEditors: 银河以北
 * @LastEditTime: 2024-09-06 16:48:14
 */


export default {
    baseURL: process.env.NODE_ENV == 'development' ? 'https://yinheyibei.serve.yinheyibei.cn/' : 'https://yinheyibei.serve.yinheyibei.cn/', // 开发环境 基本请求路径,
    baseImg: process.env.NODE_ENV == 'development' ? 'https://yinheyibei.serve.yinheyibei.cn/' : 'https://yinheyibei.serve.yinheyibei.cn/', // 开发环境 图片基础访问路径
    websocketUrl: 'ws://8.131.60.32:2348', //长链接接口
    uploadImgUrl: '/blog/common/uploadImages', // 上传图片的接口
    module: 'blog', // 上传时携带的 模块表标识
    articleTag: 'md', //md上传图片时需要携带的标识、
    // 菜单列表
    menuList: [{
        id: 1,
        name: "资源推荐",
        path: "/home",
    },
    {
        id: 2,
        name: "妙笔生花",
        path: "/resources",
    },
    {
        id: 3,
        name: "奇思妙想",
        path: "/achievements",
    },
    // {
    //     id: 4,
    //     name: "积分商城",
    //     path: "/shoppingMall",
    // }
    ]
}